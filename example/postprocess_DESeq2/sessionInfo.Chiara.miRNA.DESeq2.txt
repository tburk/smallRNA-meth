R version 3.4.1 (2017-06-30)
Platform: x86_64-apple-darwin15.6.0 (64-bit)
Running under: OS X El Capitan 10.11.6

Matrix products: default
BLAS: /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBLAS.dylib
LAPACK: /Library/Frameworks/R.framework/Versions/3.4/Resources/lib/libRlapack.dylib

locale:
[1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8

attached base packages:
[1] parallel  stats4    stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] DESeq2_1.16.1              SummarizedExperiment_1.6.5 DelayedArray_0.2.7         matrixStats_0.52.2        
 [5] Biobase_2.36.2             GenomicRanges_1.28.6       GenomeInfoDb_1.12.3        IRanges_2.10.5            
 [9] S4Vectors_0.14.7           BiocGenerics_0.22.1        openxlsx_4.0.17           

loaded via a namespace (and not attached):
 [1] genefilter_1.58.1       locfit_1.5-9.1          splines_3.4.1           lattice_0.20-35         colorspace_1.3-2       
 [6] htmltools_0.3.6         yaml_2.1.14             base64enc_0.1-3         blob_1.1.0              survival_2.41-3        
[11] XML_3.98-1.9            rlang_0.1.2             DBI_0.7                 foreign_0.8-69          BiocParallel_1.10.1    
[16] bit64_0.9-7             RColorBrewer_1.1-2      GenomeInfoDbData_0.99.0 plyr_1.8.4              stringr_1.2.0          
[21] zlibbioc_1.22.0         munsell_0.4.3           gtable_0.2.0            htmlwidgets_0.9         memoise_1.1.0          
[26] latticeExtra_0.6-28     knitr_1.17              geneplotter_1.54.0      AnnotationDbi_1.38.2    htmlTable_1.9          
[31] Rcpp_0.12.13            acepack_1.4.1           xtable_1.8-2            scales_0.5.0            backports_1.1.1        
[36] checkmate_1.8.5         Hmisc_4.0-3             annotate_1.54.0         XVector_0.16.0          bit_1.1-12             
[41] gridExtra_2.3           ggplot2_2.2.1           digest_0.6.12           stringi_1.1.5           grid_3.4.1             
[46] tools_3.4.1             bitops_1.0-6            magrittr_1.5            RSQLite_2.0             lazyeval_0.2.1         
[51] RCurl_1.95-4.8          tibble_1.3.4            Formula_1.2-2           cluster_2.0.6           Matrix_1.2-11          
[56] data.table_1.10.4-3     rpart_4.1-11            nnet_7.3-12             compiler_3.4.1         
